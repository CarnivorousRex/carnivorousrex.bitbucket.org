This is a branch of Chris Hafey's excellent Cornerstone library and the Cornerstone Demo
================

The project - (http://chafey.github.io/cornerstone)
The demo - (http://chafey.github.io/cornerstoneDemo)


Install
================

- Clone the repo and switch to its directory

- Install dependencies via [Bower](http://bower.io/):

> bower install